
$LOGFILE = "c:\logs\plugin-windows-plaxis.log"

Function Write-Log([String] $logText) {

  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append

}


Function Main {

  try {
    New-Item -ItemType Directory -Path c:\logs -Force
    Write-Log "Log directory created."
  }
  catch {
    Write-Log "$_"
    Throw $_
  }

  Write-Log "Start plugin-windows-plaxis"

  try {
    New-Item -ItemType Directory -Path c:\rsc-installation-files
    Write-Log "Log directory created."
  }
  catch {
      Write-Log "$_"
      Throw $_
  }


#MB: executable is put in research cloud in the rsc-pilot porject folder with a share link
# https://researchdrive.surfsara.nl/index.php/apps/files/?dir=/RSC%20Pilot

  Write-Log "Download installation exe"
  try {
    Invoke-RestMethod "https://researchdrive.surfsara.nl/index.php/s/ClQxIolcHVNfkaX/download" -OutFile "C:\rsc-installation-files\Setup_Plaxis2Dx64.exe" 
  }
  catch {
      Write-Log "$_"
      Throw $_
  }

  Write-Log "Open firewall for Plaxis"
  try {
    New-NetFirewallRule -DisplayName 'Plaxis 10000' -Direction Inbound -Protocol TCP -LocalPort 10000 -Action allow -RemoteAddress '145.38.0.0/16'
    New-NetFirewallRule -DisplayName 'Plaxis 10001' -Direction Inbound -Protocol TCP -LocalPort 10001 -Action allow -RemoteAddress '145.38.0.0/16'
    New-NetFirewallRule -DisplayName 'Plaxis 10022' -Direction Inbound -Protocol TCP -LocalPort 10022 -Action allow -RemoteAddress '145.38.0.0/16'    
  }
  catch {
      Write-Log "$_"
      Throw $_
  }

  #https://communities.bentley.com/products/geotech-analysis/w/plaxis-soilvision-wiki/45946/plaxis-silent-installation
  Write-Log "Install Plaxis"
  try {
    Start-Process -NoNewWindow -FilePath "C:\rsc-installation-files\Setup_Plaxis2Dx64.exe"  -ArgumentList "/s"
  }
  catch {
      Write-Log "$_"
      Throw $_
  }

  

  Write-Log "End plugin-windows-plaxis"

}




Main    

